##Usage
This code generates synthetic data from a few images through several randomized transformations. Given image bounding box and segmentation annotations, it generates a bigger synthetic dataset fro it, and arranges it in PASCAL VOC format  .

It takes a folder as input which contains the images and their corresponding annotations. It takes two kinds of annotations: bounding box annotations in XML format and segmentation annotations as segmentation maps. Additionally it takes a 'perspectives.csv' file which contains a list of tuples of points for generating the perspective transformation matrix, and a 'via_region_data.csv' file which has segmentation annotations for all the images in terms of points defining the boundaries of the objects in the images. 'via_region_data.csv' is generated through VGG Image Annotator (an online tool for creating segmentation annotations).

The user also needs to provide a value for 'N', the number of zoom+rotation transformations needed. Perspective tansformations is limited to 100 for now but can be increased by adding more points in 'perspectives.csv'.

##Directory structure:
	--Input/images/folder
		--img0.jpg (original image)
		--img0.xml (bounding box annotatoions)
		--img0.png (segmentaion map)
		--..
		--..
	--Output/images/folder
		--Annotations
		--JPEGImages
		--ImageSet
		--SegmentationMask
	--perspectives.csv
	--transformation.py
	
##To run:
	    python3 transformation.py --N=number of transformations --folder='path/to/image/folder/with/few/images'