import argparse
import cv2
import csv
import random
import numpy as np
from xml.dom import minidom
import xml.etree.ElementTree as ET
from scipy.stats import truncnorm
import os
import glob
import shutil


def perspective(image, seg_image, seg_annotations, bbox_annotations):
    seg_img = cv2.imread(seg_image)
    img = cv2.imread(image)
    annotations_file=bbox_annotations
    with open('perspectives.csv') as perspective_transformations:
        perspective_reader = csv.reader(perspective_transformations, delimiter=',')
        for m, row in enumerate(perspective_reader):
            if m == 0:
                continue
            else:
                pts = np.float32([[0, 0], [1280, 0], [0, 720], [1280, 720]])
                newpts = row[1].split(';')
                newpts_array = np.empty([len(newpts), 2], dtype=np.float32)
                for i in range(len(newpts)):
                    newpts_array[i][0] = newpts[i].split(',')[0]
                    newpts_array[i][1] = newpts[i].split(',')[1]
                M = cv2.getPerspectiveTransform(newpts_array, pts)
                transformed_seg_image = cv2.warpPerspective(seg_img, M, (1280, 720))
                transformed_image = cv2.warpPerspective(img, M, (1280, 720))
                cv2.imwrite('VOC/SegmentationMasks/' + (original_image_path.split('/'))[1].split('.')[0] + str(-m) + '.png', transformed_seg_image)
                cv2.imwrite('VOC/JPEGImages/' + (original_image_path.split('/'))[1].split('.')[0] + str(-m) + '.jpg', transformed_image)

                with open(seg_annotations) as csv_file:
                    csv_reader = csv.reader(csv_file, delimiter=',')
                    line_count = 0
                    tree = ET.parse(annotations_file)
                    root = tree.getroot()

                    for row in csv_reader:
                        if line_count == 0:
                            line_count = + 1
                        else:

                            imagepath = row[0]
                            if imagepath.split('.')[0] == (seg_image_path.split('/'))[1].split('-s')[0]:
                                polygon = row[5]
                                label_start = ':"'
                                label_end = '"'
                                object_label = (row[6].split(label_start))[1].split(label_end)[0]
                                x_start = 'x":['
                                x_end = ']'
                                x_points = (polygon.split(x_start))[1].split(x_end)[0]
                                y_start = 'y":['
                                y_end = ']'
                                y_points = (polygon.split(y_start))[1].split(y_end)[0]
                                line_count = + 1
                                x_points = x_points.split(',')
                                y_points = y_points.split(',')
                                polygon = []
                                for i in range(len(x_points)):
                                    polygon.append(int(x_points[i]))
                                    polygon.append(int(y_points[i]))
                                transformed_polygon = np.empty([len(x_points), 2], dtype=int)
                                transformed_x_points = []
                                transformed_y_points = []
                                for i in range(len(polygon) // 2):
                                    x = polygon[(i * 2)]
                                    y = polygon[(i * 2) + 1]
                                    W = np.array([[[x, y]]], dtype=np.float64)
                                    Z = np.empty([2])
                                    Z = cv2.perspectiveTransform(W, M)
                                    transformed_x_points.append(Z[0][0][0])
                                    transformed_y_points.append(Z[0][0][1])
                                outside_corner1 = (min(transformed_x_points), min(transformed_y_points))
                                outside_corner2 = (max(transformed_x_points), max(transformed_y_points))
                                nbboxpoints = []
                                for item in (outside_corner1, outside_corner2):
                                    # x checks
                                    if item[0] < 0:
                                        x = 0
                                    elif item[0] > 1280:
                                        x = 1280
                                    else:
                                        x = item[0]
                                    # y checks
                                    if item[1] < 0:
                                        y = 0
                                    elif item[1] > 720:
                                        y = 720
                                    else:
                                        y = item[1]

                                    nbboxpoints.append([x, y])
                                # print(newbboxpoints)
                                xmin = int(nbboxpoints[0][0])
                                ymin = int(nbboxpoints[0][1])
                                xmax = int(nbboxpoints[1][0])
                                ymax = int(nbboxpoints[1][1])
                                for object in (root.iter('object')):
                                    if object.find('name').text == object_label:
                                        for bbox in object.iter('bndbox'):
                                            bbox.find('xmin').text = str(xmin)
                                            bbox.find('ymin').text = str(ymin)
                                            bbox.find('xmax').text = str(xmax)
                                            bbox.find('ymax').text = str(ymax)
                                        tree.write('VOC/Annotations/' + (original_image_path.split('/'))[1].split('.')[0] + str(-m) + '.xml')

                                new_annotations_file = 'VOC/Annotations/' + (original_image_path.split('/'))[1].split('.')[0] + str(-m) + '.xml'
                                new_tree = ET.parse(new_annotations_file)
                                new_root = new_tree.getroot()
                                for object in new_root.iter('object'):
                                    for bbox in object.findall('bndbox'):
                                        xmin = int(bbox.find('xmin').text)
                                        ymin = int(bbox.find('ymin').text)
                                        xmax = int(bbox.find('xmax').text)
                                        ymax = int(bbox.find('ymax').text)
                                        if xmax - xmin < 90:
                                            new_root.remove(object)
                                            continue
                                        elif ymax - ymin < 90:
                                            new_root.remove(object)
                                            continue
                                        else:
                                            continue
                                new_tree.write('VOC/Annotations/' + (original_image_path.split('/'))[1].split('.')[0] + str(-m) + '.xml')

                                new2_annotations_file = 'VOC/Annotations/' + (original_image_path.split('/'))[1].split('.')[0] + str(-m) + '.xml'
                                new2_tree = ET.parse(new2_annotations_file)
                                new2_root = new2_tree.getroot()
                                for object2 in new2_root.iter('object'):
                                    for bbox2 in object2.findall('bndbox'):
                                        xmin = int(bbox2.find('xmin').text)
                                        ymin = int(bbox2.find('ymin').text)
                                        xmax = int(bbox2.find('xmax').text)
                                        ymax = int(bbox2.find('ymax').text)
                                        if xmax == xmin:
                                            new2_root.remove(object2)
                                            continue
                                        elif ymax == ymin:
                                            new2_root.remove(object2)
                                            continue
                                        else:
                                            continue

                                new2_tree.write('VOC/Annotations/' + (original_image_path.split('/'))[1].split('.')[0] + str(-m) +'.xml')

                                new3_annotations_file = 'VOC/Annotations/' + (original_image_path.split('/'))[1].split('.')[0] + str(-m) + '.xml'
                                new3_tree = ET.parse(new3_annotations_file)
                                new3_root = new3_tree.getroot()
                                for object3 in new3_root.iter('object'):
                                    for bbox3 in object3.findall('bndbox'):
                                        xmin = int(bbox3.find('xmin').text)
                                        ymin = int(bbox3.find('ymin').text)
                                        xmax = int(bbox3.find('xmax').text)
                                        ymax = int(bbox3.find('ymax').text)
                                        if xmax - xmin < 90:
                                            new3_root.remove(object3)
                                            continue
                                        elif ymax - ymin < 90:
                                            new3_root.remove(object3)
                                            continue
                                        else:
                                            continue

                                new3_tree.write('VOC/Annotations/' + (original_image_path.split('/'))[1].split('.')[0] + str(-m) + '.xml')
                            else:
                                continue

def rotate(image,seg_image,seg_annotations, bbox_annotations, j):
    l= j+1
    annotations_file = bbox_annotations
    # rotation
    def rotate_bound(image, angle):
        (h, w) = image.shape[:2]
        (cX, cY) = (w // 2, h // 2)
        M = cv2.getRotationMatrix2D((cX, cY), angle, 1.0)
        cos = np.abs(M[0, 0])
        sin = np.abs(M[0, 1])
        nW = int((h * sin) + (w * cos))
        nH = int((h * cos) + (w * sin))
        M[0, 2] += (nW / 2) - cX
        M[1, 2] += (nH / 2) - cY

        # perform the actual rotation and return the image
        return cv2.warpAffine(image, M, (nW, nH))

    def rotate_box(point, cx, cy, h, w, rotation_angle):
        # opencv calculates standard transformation matrix
        M = cv2.getRotationMatrix2D((cx, cy), rotation_angle, 1.0)
        # Grab  the rotation components of the matrix)
        cos = np.abs(M[0, 0])
        sin = np.abs(M[0, 1])
        # compute the new bounding dimensions of the image
        nW = int((h * sin) + (w * cos))
        nH = int((h * cos) + (w * sin))
        # adjust the rotation matrix to take into account translation
        M[0, 2] += (nW / 2) - cx
        M[1, 2] += (nH / 2) - cy
        # Prepare the vector to be transformed
        v = [point[0], point[1], 1]
        # Perform the actual rotation and return the image
        calculated = np.dot(M, v)
        rotated_point = (int(calculated[0]), int(calculated[1]))
        return rotated_point

    def corner_to_finalpoint(point):
        rotated_point = rotate_box(point, cX, cY, h, w, rotation_angle)
        scaled_point = (scale_factor * rotated_point[0], scale_factor * rotated_point[1])
        new_point = tuple((scaled_point[0] - new_origin[0], scaled_point[1] - new_origin[1]))
        return new_point

    scale_factor = np.random.uniform(1.1, 2)
    rotation_angle = np.random.uniform(-15, 45)
    img = cv2.imread(image)
    seg_img = cv2.imread(seg_image)
    rotated_image = rotate_bound(img, rotation_angle)
    rotated_seg_image = rotate_bound(seg_img, rotation_angle)
    (h, w) = img.shape[:2]
    (cX, cY) = (w // 2, h // 2)
    rescaled_image = cv2.resize(rotated_image,None, fx=scale_factor,fy=scale_factor, interpolation=cv2.INTER_LINEAR)
    rescaled_seg_image = cv2.resize(rotated_seg_image, None, fx=scale_factor, fy=scale_factor, interpolation=cv2.INTER_LINEAR)
    new_origin = random.choice([(100, 100), (260, 260), (70, 70)])
    cropped_image = rescaled_image[new_origin[0]:new_origin[0] + 720, new_origin[1]:new_origin[1] + 1280]
    cropped_seg_image = rescaled_seg_image[new_origin[0]:new_origin[0] + 720, new_origin[1]:new_origin[1] + 1280]
    cv2.imwrite('VOC/JPEGImages/' + (original_image_path.split('/'))[1].split('.')[0] + str(-l) + '.jpg', cropped_image)
    cv2.imwrite('VOC/SegmentationMasks/' + (original_image_path.split('/'))[1].split('.')[0] + str(-l) + '.png', cropped_seg_image)

    with open(seg_annotations) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        # bbox_annotations = minidom.parse(annotations_file)
        tree = ET.parse(annotations_file)
        root = tree.getroot()

        for row in csv_reader:
            if line_count == 0:
                line_count = + 1

            else:
                imagepath = row[0]
                if imagepath.split('.')[0] == (seg_image_path.split('/'))[1].split('-s')[0]:
                    polygon = row[5]
                    label_start = ':"'
                    label_end = '"'
                    object_label = (row[6].split(label_start))[1].split(label_end)[0]
                    x_start = 'x":['
                    x_end = ']'
                    x_points = (polygon.split(x_start))[1].split(x_end)[0]
                    y_start = 'y":['
                    y_end = ']'
                    y_points = (polygon.split(y_start))[1].split(y_end)[0]
                    line_count = + 1
                    x_points = x_points.split(',')
                    y_points = y_points.split(',')
                    polygon = []
                    for i in range(len(x_points)):
                        polygon.append(int(x_points[i]))
                        polygon.append(int(y_points[i]))
                    rotated_polygon = []
                    rotated_x_points = []
                    rotated_y_points = []
                    for i in range(len(polygon)//2):
                        x = polygon[(i*2)]
                        y = polygon[(i*2)+1]
                        rotated_polygon_point = corner_to_finalpoint((x, y))
                        rotated_polygon.append(rotated_polygon_point)
                        rotated_x_points.append(rotated_polygon_point[0])
                        rotated_y_points.append(rotated_polygon_point[1])
                    outside_corner1 = (min(rotated_x_points), min(rotated_y_points))
                    outside_corner2 = (max(rotated_x_points), max(rotated_y_points))
                    nbboxpoints = []
                    for item in (outside_corner1, outside_corner2):
                        # x checks
                        if item[0] < 0:
                            x = 0
                        elif item[0] > 1280:
                            x = 1280
                        else:
                            x = item[0]
                        # y checks
                        if item[1] < 0:
                            y = 0
                        elif item[1] > 720:
                            y = 720
                        else:
                            y = item[1]

                        nbboxpoints.append([x, y])
                    # print(newbboxpoints)
                    xmin = int(nbboxpoints[0][0])
                    ymin = int(nbboxpoints[0][1])
                    xmax = int(nbboxpoints[1][0])
                    ymax = int(nbboxpoints[1][1])
                    for object in (root.iter('object')):
                        if object.find('name').text == object_label:
                            for bbox in object.iter('bndbox'):
                                bbox.find('xmin').text = str(xmin)
                                bbox.find('ymin').text = str(ymin)
                                bbox.find('xmax').text = str(xmax)
                                bbox.find('ymax').text = str(ymax)
                            tree.write('VOC/Annotations/' + (original_image_path.split('/'))[1].split('.')[0] + str(-l) + '.xml')

                    new_annotations_file = 'VOC/Annotations/' + (original_image_path.split('/'))[1].split('.')[0] + str(-l) + '.xml'
                    new_tree = ET.parse(new_annotations_file)
                    new_root = new_tree.getroot()
                    for object in new_root.iter('object'):
                        for bbox in object.findall('bndbox'):
                            xmin = int(bbox.find('xmin').text)
                            ymin = int(bbox.find('ymin').text)
                            xmax = int(bbox.find('xmax').text)
                            ymax = int(bbox.find('ymax').text)
                            if xmax - xmin < 90:
                                new_root.remove(object)
                                continue
                            elif ymax - ymin < 90:
                                new_root.remove(object)
                                continue
                            else:
                                continue
                    new_tree.write('VOC/Annotations/' + (original_image_path.split('/'))[1].split('.')[0] + str(-l) + '.xml')

                    new2_annotations_file = 'VOC/Annotations/' + (original_image_path.split('/'))[1].split('.')[0] + str(-l) + '.xml'
                    new2_tree = ET.parse(new2_annotations_file)
                    new2_root = new2_tree.getroot()
                    for object2 in new2_root.iter('object'):
                        for bbox2 in object2.findall('bndbox'):
                            xmin = int(bbox2.find('xmin').text)
                            ymin = int(bbox2.find('ymin').text)
                            xmax = int(bbox2.find('xmax').text)
                            ymax = int(bbox2.find('ymax').text)
                            if xmax == xmin:
                                new2_root.remove(object2)
                                continue
                            elif ymax == ymin:
                                new2_root.remove(object2)
                                continue
                            else:
                                continue

                    new2_tree.write('VOC/Annotations/' + (original_image_path.split('/'))[1].split('.')[0] + str(-l) + '.xml')

                    new3_annotations_file = 'VOC/Annotations/' + (original_image_path.split('/'))[1].split('.')[0] + str(-l) + '.xml'
                    new3_tree = ET.parse(new3_annotations_file)
                    new3_root = new3_tree.getroot()
                    for object3 in new3_root.iter('object'):
                        for bbox3 in object3.findall('bndbox'):
                            xmin = int(bbox3.find('xmin').text)
                            ymin = int(bbox3.find('ymin').text)
                            xmax = int(bbox3.find('xmax').text)
                            ymax = int(bbox3.find('ymax').text)
                            if xmax - xmin < 90:
                                new3_root.remove(object3)
                                continue
                            elif ymax - ymin < 90:
                                new3_root.remove(object3)
                                continue
                            else:
                                continue

                    new3_tree.write('VOC/Annotations/' + (original_image_path.split('/'))[1].split('.')[0] + str(-l) + '.xml')


# scaling
def zoom(image, seg_image, seg_annotations, bbox_annotations, j):
    k = j+1
    annotations_file = bbox_annotations
    tree = ET.parse(annotations_file)
    root = tree.getroot()
    img = cv2.imread(image)
    seg_img = cv2.imread(seg_image)
    scale_factor = np.random.uniform(1.4,2)
    # scale image to twice its size
    rescaled = cv2.resize(img, None, fx = scale_factor, fy= scale_factor, interpolation=cv2.INTER_LINEAR)
    seg_rescaled = cv2.resize(seg_img, None, fx=scale_factor, fy=scale_factor, interpolation=cv2.INTER_LINEAR)
    # crop image
    corner_1 = random.choice([(50, 50), (260, 260), (470, 470),(500,500)])
    corner_2 = tuple((corner_1[0] + 720, corner_1[1] + 1280))
    cropped = rescaled[corner_1[0]:corner_2[0], corner_1[1]:corner_2[1]]
    seg_cropped = seg_rescaled[corner_1[0]:corner_2[0], corner_1[1]:corner_2[1]]

    cv2.imwrite('VOC/JPEGImages/' + (original_image_path.split('/'))[1].split('.')[0] + str(-k) + '.jpg', cropped)
    cv2.imwrite('VOC/SegmentationMasks/' + (original_image_path.split('/'))[1].split('.')[0] + str(-k) + '.png', seg_cropped)
    new_origin = corner_1
    for obj in root.iter('object'):
        for bbox in obj.iter('bndbox'):
            xmin = int(bbox.find('xmin').text)
            ymin = int(bbox.find('ymin').text)
            xmax = int(bbox.find('xmax').text)
            ymax = int(bbox.find('ymax').text)

            newbboxpoints = []
            for item in ((xmin, ymin), (xmax, ymax)):
                scaled_point = (scale_factor * item[0], scale_factor * item[1])
                new_point = tuple((scaled_point[0] - new_origin[0], scaled_point[1] - new_origin[1]))
                # x checks
                if new_point[0] < 0:
                    x = 0
                elif new_point[0] > 1280:
                    x = 1280
                else:
                    x = new_point[0]
                # y checks
                if new_point[1] < 0:
                    y = 0
                elif new_point[1] > 720:
                    y = 720
                else:
                    y = new_point[1]

                newbboxpoints.append([x,y])
            #print(newbboxpoints)
            xmin = int(newbboxpoints[0][0])
            ymin = int(newbboxpoints[0][1])
            xmax = int(newbboxpoints[1][0])
            ymax = int(newbboxpoints[1][1])
            bbox.find('xmin').text = str(xmin)
            bbox.find('ymin').text = str(ymin)
            bbox.find('xmax').text = str(xmax)
            bbox.find('ymax').text = str(ymax)
        tree.write('VOC/Annotations/' + (original_image_path.split('/'))[1].split('.')[0] + str(-k) + '.xml')

    new_annotations_file = 'VOC/Annotations/' + (original_image_path.split('/'))[1].split('.')[0] + str(-k) + '.xml'
    new_tree = ET.parse(new_annotations_file)
    new_root = new_tree.getroot()
    for object in new_root.iter('object'):
        for bbox in object.findall('bndbox'):
            xmin = int(bbox.find('xmin').text)
            ymin = int(bbox.find('ymin').text)
            xmax = int(bbox.find('xmax').text)
            ymax = int(bbox.find('ymax').text)
            if xmax-xmin < 90:
                new_root.remove(object)
                continue
            elif ymax - ymin < 90:
                new_root.remove(object)
                continue
            else:
                continue

    new_tree.write('VOC/Annotations/'  + (original_image_path.split('/'))[1].split('.')[0] + str(-k) + '.xml')

    new2_annotations_file = 'VOC/Annotations/' + (original_image_path.split('/'))[1].split('.')[0] + str(-k) + '.xml'
    new2_tree = ET.parse(new2_annotations_file)
    new2_root = new2_tree.getroot()
    for object2 in new2_root.iter('object'):
        for bbox2 in object2.findall('bndbox'):
            xmin = int(bbox2.find('xmin').text)
            ymin = int(bbox2.find('ymin').text)
            xmax = int(bbox2.find('xmax').text)
            ymax = int(bbox2.find('ymax').text)
            if xmax == xmin:
                new2_root.remove(object2)
                continue
            elif ymax == ymin:
                new2_root.remove(object2)
                continue
            else:
                continue

    new2_tree.write('VOC/Annotations/' + (original_image_path.split('/'))[1].split('.')[0] + str(-k) + '.xml')

    new3_annotations_file = 'VOC/Annotations/' + (original_image_path.split('/'))[1].split('.')[0] + str(-k) + '.xml'
    new3_tree = ET.parse(new3_annotations_file)
    new3_root = new3_tree.getroot()
    for object3 in new3_root.iter('object'):
        for bbox3 in object3.findall('bndbox'):
            xmin = int(bbox3.find('xmin').text)
            ymin = int(bbox3.find('ymin').text)
            xmax = int(bbox3.find('xmax').text)
            ymax = int(bbox3.find('ymax').text)
            if xmax - xmin < 90:
                new3_root.remove(object3)
                continue
            elif ymax - ymin < 90:
                new3_root.remove(object3)
                continue
            else:
                continue

    new3_tree.write('VOC/Annotations/' + (original_image_path.split('/'))[1].split('.')[0] + str(-k) + '.xml')


def motion_blur(img):
    size = 15
    # generating the kernel
    kernel_motion_blur = np.zeros((size, size))
    kernel_motion_blur[int((size - 1) / 2), :] = np.ones(size)
    kernel_motion_blur = kernel_motion_blur / size

    # applying the kernel to the input image
    output = cv2.filter2D(img, -1, kernel_motion_blur)
    return output


def gaussian_blur(img):
    blur = cv2.GaussianBlur(img, (5, 5),0)
    return blur


def gaussian_noise(img):
    row, col, ch = img.shape
    mean = 0
    var = 0.1
    sigma = var ** 0.5
    gauss = np.random.normal(mean, sigma, (row, col, ch))
    gauss = gauss.reshape(row, col, ch)
    noisy = img + gauss
    return noisy


def change_contrast(img):
    new_image = np.zeros(img.shape, img.dtype)
    alpha = np.random.uniform(1.0,2.2)
    beta = 0
    new_image = cv2.convertScaleAbs(img, alpha=alpha, beta=beta)
    return new_image


def change_color(img):
    k = random.choice((0, 1, 2))
    factor = np.random.uniform(-0.05, 1.2)
    print(factor)
    bright = np.full((img.shape[0], img.shape[1], 3), 255)
    dark = np.full((img.shape[0], img.shape[1], 3), 0)
    img[:, :, k] = np.multiply(img[:, :, k], factor)
    temp1 = np.minimum(bright, img)
    temp2 = np.maximum(dark, temp1)
    return temp2


parser = argparse.ArgumentParser()
parser.add_argument('--folder', metavar='--folder_path', type=str,
                        help='path to images folder')
parser.add_argument('--N', metavar='--N', help='number of transformations', type=int)

args = parser.parse_args()

n = args.N
folder_path = args.folder


def string_to_function(argument):
    switcher = {
        1: rotate,
        2: zoom

        }
    return switcher.get(argument)


def string_to_noise(noise_type):
    noise_switcher = {
        1: gaussian_noise,
        2: gaussian_blur,
        3: motion_blur

        }
    return noise_switcher.get(noise_type)


def string_to_further_transform(transform_type):
        transform_switcher = {
            1: change_color,
            2: change_contrast

        }
        return transform_switcher.get(transform_type)

os.makedirs('VOC/JPEGImages')
os.makedirs('VOC/Annotations')
os.makedirs('VOC/SegmentationMasks')
os.makedirs('VOC/ImageSet')


for image in glob.glob(folder_path + '*.jpg'):
    orig_imgpath = image
    image_id = orig_imgpath.split('.')[0]
    original_image_path = orig_imgpath
    seg_image_path = image_id + '-segmented.png'
    seg_annotations_path = folder_path + 'via_region_data.csv'
    bbox_annotations_path = image_id + '.xml'

    for j in range(100, n + 100):
        # zoom(original_image_path, seg_image_path, seg_annotations_path, bbox_annotations_path, j)
        # Get the function from switcher dictionary
        trans_func = string_to_function(random.choice((1, 2)))
        # call to function
        trans_func(original_image_path, seg_image_path, seg_annotations_path, bbox_annotations_path, j)
        # function saves transformed image, bitmap and XML file

    perspective(original_image_path, seg_image_path, seg_annotations_path, bbox_annotations_path)



for image in glob.glob('VOC/JPEGImages/*.jpg'):
    imgpath = image
    imgpath_start = 'JPEGImages/'
    imgpath_end = '.'
    id = (imgpath.split(imgpath_start))[1].split(imgpath_end)[0]
    img = cv2.imread(imgpath)
    if img.shape == (720,1280,3):
        noise_func = string_to_noise(random.choice((1, 2, 3)))
        noise_image = noise_func(img)
        further_transforms = random.choice((0, 1, 2))
        if further_transforms == 0:
            pass
        elif further_transforms == 1:
            further_func = string_to_further_transform(random.choice((1, 2)))
            noise_image = further_func(noise_image)
        else:
            noise_image = change_color(noise_image)
            noise_image = change_contrast(noise_image)

        cv2.imwrite('VOC/JPEGImages/' + id + '-0.jpg', noise_image)
        shutil.copy('VOC/Annotations/'+ id + '.xml','VOC/Annotations/'+ id + '-0.xml')
        shutil.copy('VOC/SegmentationMasks/' + id + '.png','VOC/SegmentationMasks/' + id + '-0.png')
        continue
    else:
        print('Deleting......')
        print(imgpath)
        os.remove(imgpath)
        if os.path.exists('VOC/Annotations/'+ id + '.xml'):
            os.remove('VOC/Annotations/'+ id + '.xml')
        os.remove('VOC/SegmentationMasks/' + id + '.png')


for file in glob.glob(folder_path + '*'):
    filepath = file
    filepath_start = folder_path
    filepath_end = '.'
    file_id = (filepath.split(filepath_start))[1].split(filepath_end)[0]
    if filepath.split('.')[1] == '.jpg':
        shutil.copy(filepath, 'VOC/JPEGImages/' + file_id + '.jpg')
    if filepath.split('.')[1] == '.xml':
        shutil.copy(filepath, 'VOC/Annotations/' + file_id + '.xml')
    if filepath.split('.')[1] == '.png':
        shutil.copy(filepath, 'VOC/SegmentationMasks/' + file_id + '.png')