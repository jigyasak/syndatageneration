# creates labalmap.pbtxt from labels.txt

import argparse

def create_map(labelf, labelmapf):
    with open(labelf) as f1, open(labelmapf,"a") as f2:
        # get values from labels.txt
        for line in f1:
            id=line.split(" ")[0]
            label=line.split(" ")[1]
	    # create labelmap	
            if id=="0":
                continue
            else:
		# insert values from class name and id
                label = "'" + label.rstrip() + "'"
                f2.write("item {" + "\n")
		f2.write("  id: " + id + "\n")
		f2.write("  name: " + label + "\n")
		f2.write("}" + "\n")
                f2.write("\n")


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--label",help="The file with label names")
    parser.add_argument("--labelmap",help="The file to be created with label names")
    args = parser.parse_args()
    labelfile=args.label
    labelmapfile=args.labelmap
    create_map(labelfile, labelmapfile)