# GLOBAL CONFIG FILE

# PATHS
INPUT_PATH=/home/katrolia/cache/temp4/ # path to directory containing all images and a single segmentation .csv file created from VGG Annotator tool: http://www.robots.ox.ac.uk/~vgg/software/via/via.html 
FOLDER_PATH=/home/katrolia/cache/temp4_dir/  # path where class folders will be created
OUTPUT_PATH=/home/katrolia/cache/output_temp4/ # path to directory where VOC dataset will be created
DATA_PATH=/home/katrolia/temp_model/data/  # path where training data (labelmap.pbtxt, train.record and val.record) will be generated
WEIGHTS_PATH=/home/katrolia/temp_model/weights/  # path where model weights after training will be stored
NUM=1 # parameter for number of images to be generated

# CHANGES MADE HERE WILL BE REFLECTED IN default.py
BACKGROUND_DIR="\/home\/katrolia\/SynDataGenerator\/Generate\/backgrounds\/"
BACKGROUND_GLOB_STRING='"*.png"'
POISSON_BLENDING_DIR="\/home\/katrolia\/SynDataGenerator\/Generate\/"
SELECTED_LIST_FILE="\/home\/katrolia\/SynDataGenerator\/Generate\/selected.txt"
DISTRACTOR_LIST_FILE="\/home\/katrolia\/SynDataGenerator\/Generate\/neg_list.txt"
DISTRACTOR_DIR="\/home\/katrolia\/SynDataGenerator\/Generate\/distractor_objects\/"
DISTRACTOR_GLOB_STRING='"*.jpg"'
INVERTED_MASK=True # Set to true if white pixels represent background
NUMBER_OF_WORKERS=4
BLENDING_LIST="['gaussian','poisson', 'none', 'box', 'motion']"
MIN_NO_OF_OBJECTS=1
MAX_NO_OF_OBJECTS=5
MIN_NO_OF_DISTRACTOR_OBJECTS=0
MAX_NO_OF_DISTRACTOR_OBJECTS=2
WIDTH=640
HEIGHT=480
MAX_ATTEMPTS_TO_SYNTHESIZE=2
MIN_SCALE=0.27 # min scale for scale augmentation
MAX_SCALE=0.7 # max scale for scale augmentation
MAX_DEGREES=30 # max rotation allowed during rotation augmentation
MAX_TRUNCATION_FRACTION=0.25 # max fraction to be truncated = MAX_TRUNCACTION_FRACTION*(WIDTH/HEIGHT)
MAX_ALLOWED_IOU=0.25 # IOU > MAX_ALLOWED_IOU is considered an occlusion
MIN_WIDTH=6 # Minimum width of object to use for data generation
MIN_HEIGHT=6 # Minimum height of object to use for data generation

# PATH TO TENSORFLOW OBJECT DETECTION API research/object_detection/model_main.py
TFODAPI_PATH=/usr/local/lib/python2.7/dist-packages/tensorflow/models-master/research/object_detection/model_main.py

# ARGUMENTS FOR TRAINING THE MODEL
PIPELINE_CONFIG_PATH=/home/katrolia/SynDataGenerator/Generate/model/cancer_ssd_exp1.config  # path to pipeline config
MODEL_DIR=$WEIGHTS_PATH  # path where model will be saved
NUM_TRAIN_STEPS=50000
SAMPLE_1_OF_N_EVAL_EXAMPLES=1

MODEL_CKPT=/home/katrolia/SynDataGenerator/Generate/model/model.ckpt  # path to model checkpoint