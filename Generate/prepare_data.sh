#!/bin/bash
source config.sh

# create folders if they do not exist
for path in $FOLDER_PATH $OUTPUT_PATH $DATA_PATH $WEIGHTS_PATH
do
    #check if output path exists
    echo $path
    if [ ! -d "$path" ]
    then
        mkdir $path
    fi
done

# create class folders
echo "====================================================CREATING DATA FOLDERS===================================================="
python create_obj_dirs.py --input_dir=$INPUT_PATH --output_dir=$FOLDER_PATH

# find total number of classes
MAX_NO_OF_OBJECTS="$(find $FOLDER_PATH -maxdepth 1 -mindepth 1 -type d -name '*' |  wc -l)"
echo "====================================================GENERATING OBJECT MASKS=================================================="
find $FOLDER_PATH -maxdepth 1 -mindepth 1 -type d -name '*' |  
while read filename;do 
# generate masks
python mask_pbm.py --folder=$filename
done


# modify selected.txt
#find $INPUT_PATH -maxdepth 1 -mindepth 1 -type d -name '*' |  
#while read filename;do
#echo $(basename "$filename") >> $(dir selected.txt)
#done 

#modify neg_list.txt
#find $DISTRACTOR_DIR -maxdepth 1 -mindepth 1 -type d -name '*' | 
#while read filename
#do echo $(basename "$filename") >> $(dir neg_list.txt)
#done

# copying values given by user in config.sh to default.py, which will be used in dataset_generator.py
echo "====================================================MODIFYING default.py====================================================="
sed -E -i "s/^(BACKGROUND_DIR[[:blank:]]*=[[:blank:]]*).*/\1$BACKGROUND_DIR/" default.py
sed -E -i "s/^(BACKGROUND_GLOB_STRING[[:blank:]]*=[[:blank:]]*).*/\1$BACKGROUND_GLOB_STRING/" default.py
sed -E -i "s/^(POISSON_BLENDING_DIR[[:blank:]]*=[[:blank:]]*).*/\1$POISSON_BLENDING_DIR/" default.py
sed -E -i "s/^(SELECTED_LIST_FILE[[:blank:]]*=[[:blank:]]*).*/\1$SELECTED_LIST_FILE/" default.py
sed -E -i "s/^(DISTRACTOR_LIST_FILE[[:blank:]]*=[[:blank:]]*).*/\1$DISTRACTOR_LIST_FILE/" default.py
sed -E -i "s/^(DISTRACTOR_DIR[[:blank:]]*=[[:blank:]]*).*/\1$DISTRACTOR_DIR/" default.py
sed -E -i "s/^(DISTRACTOR_GLOB_STRING[[:blank:]]*=[[:blank:]]*).*/\1$DISTRACTOR_GLOB_STRING/" default.py
sed -E -i "s/^(INVERTED_MASK[[:blank:]]*=[[:blank:]]*).*/\1$INVERTED_MASK/" default.py
sed -E -i "s/^(NUMBER_OF_WORKERS[[:blank:]]*=[[:blank:]]*).*/\1$NUMBER_OF_WORKERS/" default.py
sed -E -i "s/^(BLENDING_LIST[[:blank:]]*=[[:blank:]]*).*/\1$BLENDING_LIST/" default.py
sed -E -i "s/^(MIN_NO_OF_OBJECTS[[:blank:]]*=[[:blank:]]*).*/\1$MIN_NO_OF_OBJECTS/" default.py
sed -E -i "s/^(MAX_NO_OF_OBJECTS[[:blank:]]*=[[:blank:]]*).*/\1$MAX_NO_OF_OBJECTS/" default.py
sed -E -i "s/^(MIN_NO_OF_DISTRACTOR_OBJECTS[[:blank:]]*=[[:blank:]]*).*/\1$MIN_NO_OF_DISTRACTOR_OBJECTS/" default.py
sed -E -i "s/^(MAX_NO_OF_DISTRACTOR_OBJECTS[[:blank:]]*=[[:blank:]]*).*/\1$MAX_NO_OF_DISTRACTOR_OBJECTS/" default.py
sed -E -i "s/^(\<WIDTH\>[[:blank:]]*=[[:blank:]]*).*/\1$WIDTH/" default.py
sed -E -i "s/^(HEIGHT[[:blank:]]*=[[:blank:]]*).*/\1$HEIGHT/" default.py
sed -E -i "s/^(MAX_ATTEMPTS_TO_SYNTHESIZE[[:blank:]]*=[[:blank:]]*).*/\1$MAX_ATTEMPTS_TO_SYNTHESIZE/" default.py
sed -E -i "s/^(MIN_SCALE[[:blank:]]*=[[:blank:]]*).*/\1$MIN_SCALE/" default.py
sed -E -i "s/^(MAX_SCALE[[:blank:]]*=[[:blank:]]*).*/\1$MAX_SCALE/" default.py
sed -E -i "s/^(MAX_DEGREES[[:blank:]]*=[[:blank:]]*).*/\1$MAX_DEGREES/" default.py
sed -E -i "s/^(MAX_TRUNCATION_FRACTION[[:blank:]]*=[[:blank:]]*).*/\1$MAX_TRUNCATION_FRACTION/" default.py
sed -E -i "s/^(MAX_ALLOWED_IOU[[:blank:]]*=[[:blank:]]*).*/\1$MAX_ALLOWED_IOU/" default.py
sed -E -i "s/^(MIN_WIDTH[[:blank:]]*=[[:blank:]]*).*/\1$MIN_WIDTH/" default.py
sed -E -i "s/^(MIN_HEIGHT[[:blank:]]*=[[:blank:]]*).*/\1$MIN_HEIGHT/" default.py
sed -i 's;BACKGROUND_DIR=;BACKGROUND_DIR=";' default.py
sed -i 1's/$/" &/' default.py
sed -i 's;POISSON_BLENDING_DIR=;POISSON_BLENDING_DIR=";' default.py
sed -i 2's/$/" &/' default.py
sed -i 's;SELECTED_LIST_FILE=;SELECTED_LIST_FILE=";' default.py
sed -i 3's/$/" &/' default.py
sed -i 's;DISTRACTOR_LIST_FILE=;DISTRACTOR_LIST_FILE=";' default.py
sed -i 4's/$/" &/' default.py
sed -i 's;DISTRACTOR_DIR=;DISTRACTOR_DIR=";' default.py
sed -i 5's/$/" &/' default.py

# run dataset_generator.py to generate new images from inputs
echo "====================================================GENERATING NEW DATASET FROM dataset_generator.py========================="
/usr/bin/python dataset_generator.py ${FOLDER_PATH} ${OUTPUT_PATH} --num=${NUM} #--selected --rotation --scale --dontocclude --add_distractors


