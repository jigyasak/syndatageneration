#!/bin/bash
source config.sh

VAR1="\"ckpt\""
VAR2="\"trainrecord\""
VAR3="\"valrecord\""
VAR4="\"labelmappbtxt\""

LABEL='labels.txt'
LABEL_PATH="$OUTPUT_PATH${LABEL}"
LABELMAP='labelmap.pbtxt'
LABELMAP_PATH="$DATA_PATH${LABELMAP}"
TRAINRECORD='pascal_train.record'
TRAINRECORD_PATH="$DATA_PATH${TRAINRECORD}"
VALRECORD='pascal_val.record'
VALRECORD_PATH="$DATA_PATH${VALRECORD}"

# create label_map.pbtxt
echo "====================================================CREATING LABELMAP========================================================"
touch $LABELMAP_PATH
/usr/bin/python create_labelmap.py --label=$LABEL_PATH --labelmap=$LABELMAP_PATH

# run record.py for train and test folders
echo "====================================================CREATING TRAIN AND VALIDATION RECORDS===================================="
/usr/bin/python create_pascal_tf_record.py \
    --label_map_path=$LABELMAP_PATH \
    --data_dir=$OUTPUT_PATH --year=VOC2007 --set=train \
    --output_path=$TRAINRECORD_PATH

/usr/bin/python create_pascal_tf_record.py \
    --label_map_path=$LABELMAP_PATH \
    --data_dir=$OUTPUT_PATH --year=VOC2007 --set=val \
    --output_path=$VALRECORD_PATH



# modify model config with paths and number of classes
# number of classes
echo "====================================================MODIFYING THE MODEL CONFIG FILE=========================================="
NUM_CLASSES="$(find $INPUT_PATH -maxdepth 1 -mindepth 1 -type d -name '*' |  wc -l)"
#echo 
sed -E -i "s/^([[:blank:]]*num_classes:[[:blank:]]*).*/\1$NUM_CLASSES/" $PIPELINE_CONFIG_PATH
# checkpoint
sed -E -i "s@^([[:blank:]]*fine_tune_checkpoint:[[:blank:]]*).*@\1$VAR1@" "$PIPELINE_CONFIG_PATH"
sed -E -i "s@ckpt@$MODEL_CKPT@" "$PIPELINE_CONFIG_PATH"
# train.record
sed -E -i "0,/[[:blank:]]*input_path:[[:blank:]]/ s@^([[:blank:]]*input_path:[[:blank:]]).*@\1$VAR2@" $PIPELINE_CONFIG_PATH
sed -E -i "s@trainrecord@$TRAINRECORD_PATH@" "$PIPELINE_CONFIG_PATH"
# val.record
sed -E -i "0,/[[:blank:]]*input_path:[[:blank:]]/! {0,/[[:blank:]]*input_path:[[:blank:]]/ s@^([[:blank:]]*input_path:[[:blank:]]).*@\1$VAR3@}" $PIPELINE_CONFIG_PATH
sed -E -i "s@valrecord@$VALRECORD_PATH@" "$PIPELINE_CONFIG_PATH"
# labelmap.pbtxt
sed -E -i "s@^([[:blank:]]*label_map_path:[[:blank:]]*).*@\1$VAR4@" $PIPELINE_CONFIG_PATH
sed -E -i "s@labelmappbtxt@$LABELMAP_PATH@" "$PIPELINE_CONFIG_PATH"


# train
echo "====================================================TRAINING================================================================="
/usr/bin/python $TFODAPI_PATH --pipeline_config_path=$PIPELINE_CONFIG_PATH --train_dir=$MODEL_DIR --num_train_steps=$NUM_TRAIN_STEPS --sample_1_of_n_eval_examples=$SAMPLE_1_OF_N_EVAL_EXAMPLES --alsologtostderr     
 

# optional evaluate simultaneously
# tensorboard --logdir=${MODEL_DIR}

