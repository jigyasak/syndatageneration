## Scripts to automate synthetic dataset generation and training. 

This folder contains scripts and programs to automate the procedure of generation and preparation of synthetic imageset from a few images and their annotations, and training of a model from that generated datset using Tensorflow Object Detection API.

### Usage:
1. User first annotates a dataset using VGG Annotaion tool: http://www.robots.ox.ac.uk/~vgg/software/via/via.html .
2. Edit config.sh to reflect the correct paths for data folders and arguments for training and dataset_generator.py.
3. Run prepare_dataset.sh script to generate synthetic dataset:
    bash ./prepare_dataset.sh
3. Run train.sh script to create training records and labelmap, and start model training:
    bash ./train.sh