# This program generates segmentation masks for images in the folder provided
# in the arguments. The mask is generated in .pbm format.


import numpy
import PIL
from PIL import Image, ImageDraw
import scipy.misc
import csv
import argparse
import os.path
import glob

parser = argparse.ArgumentParser()
parser.add_argument('--folder', help='folder containing images and segmentation data, using which masks will be generated')
args = parser.parse_args()

annotations_folder = args.folder

for file in glob.glob(annotations_folder + '/*.csv'):
    with open(file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                line_count = + 1
            else:
                imagepath = row[0]
                # check if folder contains image file or not
                if os.path.exists(annotations_folder + '/' + imagepath):
                    polygon = row[5]
                    x_start = 'x":['
                    x_end = ']'
                    x_points = (polygon.split(x_start))[1].split(x_end)[0]
                    y_start = 'y":['
                    y_end = ']'
                    y_points = (polygon.split(y_start))[1].split(y_end)[0]
                    line_count = + 1
                    x_points = x_points.split(',')
                    y_points = y_points.split(',')
                    polygon = []
                    for i in range(len(x_points)):
                        polygon.append(int(x_points[i]))
                        polygon.append(int(y_points[i]))

                    if os.path.isfile(annotations_folder + '/' + imagepath.split('.')[0] + '.pbm') is False:
                        #im = Image.open(annotations_folder + '/' + imagepath
                        with Image.open(annotations_folder + '/' + imagepath) as im:
                            width,height = im.size
                        img = Image.new('1', (width, height), 1)

                    else:
                        img = Image.open(annotations_folder + '/' + imagepath.split('.')[0] + '.pbm')

                    ImageDraw.Draw(img).polygon(polygon, outline=1, fill=0)
                    mask = numpy.array(img)
                    scipy.misc.imsave(annotations_folder + '/' + imagepath.split('.')[0] + '.pbm', mask)
                

