BACKGROUND_DIR="/home/katrolia/SynDataGenerator/Generate/backgrounds/" 
POISSON_BLENDING_DIR="/home/katrolia/SynDataGenerator/Generate/" 
SELECTED_LIST_FILE="/home/katrolia/SynDataGenerator/Generate/selected.txt" 
DISTRACTOR_LIST_FILE="/home/katrolia/SynDataGenerator/Generate/neg_list.txt" 
DISTRACTOR_DIR="/home/katrolia/SynDataGenerator/Generate/distractor_objects/" 
BACKGROUND_GLOB_STRING="*.png"
DISTRACTOR_GLOB_STRING="*.jpg"
INVERTED_MASK=True

NUMBER_OF_WORKERS=4
BLENDING_LIST=['gaussian','poisson', 'none', 'box', 'motion']
MIN_NO_OF_OBJECTS=1
MAX_NO_OF_OBJECTS=2
MIN_NO_OF_DISTRACTOR_OBJECTS=0
MAX_NO_OF_DISTRACTOR_OBJECTS=2
WIDTH=640
HEIGHT=480
MAX_ATTEMPTS_TO_SYNTHESIZE=2
MIN_SCALE=0.27
MAX_SCALE=0.7
MAX_DEGREES=30
MAX_TRUNCATION_FRACTION=0.25
MAX_ALLOWED_IOU=0.25
MIN_WIDTH=6
MIN_HEIGHT=6
