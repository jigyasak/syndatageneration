# This program creates a separate directory for each class, which will 
# contain the images for that class and the segmentation annotation file
# 'via_region_data.csv'. The masks will also be created in those folders. 

from xml.dom import minidom
import xml.etree.ElementTree as ET
import os
import shutil
import glob
import argparse
import csv

def create_folders(path):
    anno_path = path + 'via_region_data.csv'
    with open (anno_path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        labels = []
        for row in csv_reader:
            if line_count == 0:
                line_count = + 1
                #print(row)
            else:
                #print(row)
                label = (row[6].split(":\"")[1].split("\"")[0])
                if label not in labels:
                    labels.append(label) 
                    print(label)  
                if not os.path.exists(output_path + label):
                    os.makedirs(output_path + label)
                    print('folder created')
                img_file = row[0]
                print(img_file)
                if not os.path.exists(output_path + label + '/' + img_file):
                    if os.path.exists(input_path + img_file):
                        print('image copied')
                        shutil.copy((input_path + img_file),(output_path + label))
                    else:
                        continue
    for label in labels:
        print('annotation copied')
        shutil.copy(anno_path,(output_path + label))
            
      
if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_dir",help="The directory with all images and their segmentation annotations")
    parser.add_argument("--output_dir",help="The directory where the class folders will be created")
    args = parser.parse_args()
    input_path = args.input_dir
    output_path = args.output_dir
    create_folders(input_path)